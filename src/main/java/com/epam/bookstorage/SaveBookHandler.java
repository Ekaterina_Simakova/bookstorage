package com.epam.bookstorage;

import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_ROOT_VALUE;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.UUID;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The handler retrieves {@link Book} in JSON format and stores it in DynamoDB and S3 services.
 */
public class SaveBookHandler implements RequestStreamHandler
{
	private static Properties configProperties;

	static
	{
		configProperties = new Properties();
		try(FileInputStream fis = new FileInputStream("config.properties"))
		{
			configProperties.load(fis);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

	private AmazonS3 s3Client;
	private AmazonDynamoDB dynamoDBClient;

	public SaveBookHandler()
	{
		s3Client = AmazonS3ClientBuilder.defaultClient();
		dynamoDBClient = AmazonDynamoDBClientBuilder.defaultClient();
	}

	@Override
	public void handleRequest(InputStream is, OutputStream os, Context context) throws IOException
	{
		LambdaLogger logger = context.getLogger();

		try
		{
			byte[] content = IOUtils.toByteArray(is);

			UUID bookID = storeInDynamoDB(content);
			storeInS3(bookID, content);

			os.write(bookID.toString().getBytes());
		}
		catch (IOException e)
		{
			logger.log("There is an error with InputStream reading: " + e.getMessage());
			throw new IOException("There is an issue with request content reading.", e);
		}
	}

	/**
	 * Converts the content to Book entity and saves it in DynamoDB table.
	 *
	 * @param content
	 * @return
	 * @throws IOException
	 */
	private UUID storeInDynamoDB(byte[] content) throws IOException
	{
		ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.enable(UNWRAP_ROOT_VALUE);
		Book book = jsonMapper.readValue(new ByteArrayInputStream(content), Book.class);

		DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDBClient);

		dynamoDBMapper.save(book);
		return book.getID();
	}

	/**
	 * Stores json file with specified content in S3.
	 *
	 * @param bookID
	 * @param content
	 * @return
	 */
	private String storeInS3(UUID bookID, byte[] content)
	{
		String destFileName = new StringBuilder()
				.append(configProperties.getProperty("s3.destination.uploads.folder"))
				.append("/")
				.append(bookID)
				.append(".json")
				.toString();
		s3Client.putObject(configProperties.getProperty("s3.destination.bucket"), destFileName, new ByteArrayInputStream(content),
				new ObjectMetadata());

		return destFileName;
	}
}