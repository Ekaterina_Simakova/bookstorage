package com.epam.bookstorage;

import java.util.Date;
import java.util.UUID;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "book")
@DynamoDBTable(tableName = "Book")
public class Book
{
	private UUID ID;
	private String title;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
	private Date year;
	private String description;
	private String author;

	@DynamoDBHashKey(attributeName = "ID")
	@DynamoDBAutoGeneratedKey
	public UUID getID()
	{
		return ID;
	}

	public void setID(UUID ID)
	{
		this.ID = ID;
	}

	@DynamoDBAttribute
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	@DynamoDBAttribute
	public Date getYear()
	{
		return year;
	}

	public void setYear(Date year)
	{
		this.year = year;
	}

	@DynamoDBAttribute
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@DynamoDBAttribute
	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}
}
